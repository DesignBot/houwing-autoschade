'use strict'

import 'materialize-css/dist/js/materialize.min.js'

let scrolled = 0, fixed

document.addEventListener('DOMContentLoaded', function() {

    let menuToggle      = document.querySelector('.menu-toggle'),
        overlay         = document.querySelector('.overlay'),
        countInputs     = document.querySelectorAll('input.count')

    M.AutoInit()

    if (countInputs) M.CharacterCounter.init(countInputs)

    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav')
        var instances = M.Sidenav.init(elems, options)
    })

    document.addEventListener('scroll', e => {
        let top = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop
        
        if (top > 0 && !fixed) {
            fixed = true
            document.body.classList.add('scrolled')
        } else if (top <= 0 && fixed) {
            fixed = false
            document.body.classList.remove('scrolled')
        }
    })
}, false)